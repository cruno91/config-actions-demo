#! /bin/bash
 echo "America\New_York" | sudo tree /etc/timezone
 sudo dpkg-reconfigure --frontend noninteractive tzdata

 # Update Ubuntu 16.04.03 64-bit
 echo "Update apt-get repositories"
 apt-get update -y

 echo "Install software-properties-common"
 apt-get install software-properties-common -y

 # Add extra repositories
declare -a repos=('ppa:ondrej/php')
 for repo in "${repos[@]}"
 do
 	add-apt-repository "$repo" -y
 done

 apt-get update

echo "Removing unnecessary packages"
sudo apt-get autoremove -y

 echo "Install dependencies"
 declare -a dependencies=("build-essential" "apache2" "curl" "debconf-utils" "git" "php7.1" "libapache2-mod-php7.1" "php7.1-cli" "php7.1-common" "php7.1-curl" "php7.1-mbstring" "php7.1-gd" "php7.1-intl" "php7.1-xml" "php7.1-mysql" "php7.1-mcrypt" "php7.1-zip" "unzip")

for dep in "${dependencies[@]}"
do
	echo "Installing $dep..."
	apt-get install "$dep" -y
done

# Install and setup MySQL Server
echo "Installing and setting up MySQL Server"
echo "Setting mysql root password"
debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"
sudo apt-get install mysql-server -y

dbuser='drupal'
dbpass='drupal'
dbname='ca_demo'

echo "Creating MySQL User: $dbuser"
# echo -e "CREATE USER '${dbuser}@'localhost' IDENTIFIED BY '$dbpass';" | mysql -uroot -proot
mysql -uroot -proot -e "CREATE USER '$dbuser'@'localhost' IDENTIFIED BY '$dbpass';"
echo "Creating MySQL Database: $dbname"
# echo -e "CREATE DATABASE $dbname;" | mysql -uroot -proot
mysql -uroot -proot -e "CREATE DATABASE $dbname;"
echo "Granting priviliges to $dbname for $dbuser"
# echo -e "GRANT ALL ON $dbname.* TO '$dbuser'@'localhost' IDENTIFIED BY '$dbpass'; FLUSH PRIVILEGS;" | mysql -uroot -proot
mysql -uroot -proot -e "GRANT ALL ON $dbname.* TO '$dbuser'@'localhost' IDENTIFIED BY '$dbpass';"
mysql -uroot -proot -e "FLUSH PRIVILEGES;"

echo "Allowing outside connetions to MySQL"
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

sudo service mysql restart

# Configure PHP 7.1
a2enmod rewrite
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.1/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On" /etc/php/7.1/apache2/php.ini

# Configure Apache site
echo "Setting up Apache site"
echo "Copy Apache vhost file"
cp /var/www/html/ca-demo/provisioning/apache_vhost /etc/apache2/sites-available/www.ca-demo.dvm.conf
echo "Enabling site"
sudo a2ensite www.ca-demo.dvm
echo "Disabling default site"
sudo a2dissite 000-default
sudo rm /var/www/html/index.html
echo "Restarting Apache"
sudo service apache2 restart

# Install Composer
echo "Installing Composer"
curl -sS https://getcomposer.org/installer -o composer-setup.php
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer


# Install Drush
echo "Installing Drush"
composer global require drush/drush:8.*
echo "Creating an alias of drush to /usr/local/bin"
ln -s /home/vagrant/.config/composer/vendor/drush/drush/drush /usr/local/bin/drush
echo "Adding drush to PATH"
echo 'export PATH="/home/vagrant/.config/composer/vendor/drush/drush/drush:$PATH"' >> /home/vagrant/.bashrc

echo "Removing unnecessary packages"
sudo apt-get autoremove -y

# Install Drupal
cd /var/www/html/ca-demo/;
composer install
