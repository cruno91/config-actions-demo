<?php

namespace Drupal\pwc_access_denied\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * PWC Access Denied Config form.
 */
class AccessDeniedConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pwc_access_denied_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pwc_access_denied.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['access_denied_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Title'),
      '#default_value' => \Drupal::config('pwc_access_denied.settings')->get('access_denied_title'),
    ];

    $form['access_denied_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Access Denied message'),
      '#format' => 'rich_text',
      '#description' => $this->t('Text that will be shown on the Access Denied (403) page.'),
      '#default_value' => \Drupal::config('pwc_access_denied.settings')->get('access_denied_text'),
    ];

    $form['include_sso_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include PWC SSO login button on Access Denied page'),
      '#default_value' => \Drupal::config('pwc_access_denied.settings')->get('include_sso_login'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration, set values and save config.
    \Drupal::configFactory()->getEditable('pwc_access_denied.settings')
      ->set('include_sso_login', $form_state->getValue('include_sso_login'))
      ->set('access_denied_text', $form_state->getValue(['access_denied_text', 'value']))
      ->set('access_denied_title', $form_state->getValue('access_denied_title'))
      ->save();

    \Drupal::service('cache_tags.invalidator')->invalidateTags(['pwcsf_settings']);

    parent::submitForm($form, $form_state);
  }

}
